//
//  MainScene.h
//  ExpJam
//
//  Created by Two Tails on 09/12/2014.
//
//

#ifndef __MAIN_SCENE_H__
#define __MAIN_SCENE_H__

#include "cocos2d.h"


class MainScene : public cocos2d::Scene
{
    typedef cocos2d::Scene Super;
public:
    
    enum DangerLevel
    {
        save = 0,
        dangerous,
        fatal
    };
    
    struct FallingItem
    {
        FallingItem(cocos2d::Sprite* sp, DangerLevel l) : sprite(sp), level(l) , checked(false){}
        DangerLevel level;
        cocos2d::Sprite* sprite;
        bool checked;
    };
    
    // implement the "static create()" method manually
    CREATE_FUNC(MainScene);

    // scene initialisation
    virtual bool init();
    // scene shown
    virtual void onEnter();
    // scene hidden
    virtual void onExit();
    
    // called once per frame
    virtual void update( float delta );
    
    //called for every fallen item to make it fall and rotate infinitely (recursion callback)
    void fallingItemsAction(FallingItem* sp);
    
    static cocos2d::Scene* createScene();
    
    // key pressed event
    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    // key lifted event
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    
    // mouse pressed down
    void onMouseDown( cocos2d::Event *event );
    // mouse button lifted
    void onMouseUp( cocos2d::Event *event );
    // mouse moved
    void onMouseMove( cocos2d::Event *event );
    // mouse scroll
    void onMouseScroll( cocos2d::Event *event );
    
    
private:
    
    cocos2d::EventListenerKeyboard* _keyEventListener = NULL;
    cocos2d::EventListenerMouse* _mouseEventListener = NULL;
    cocos2d::Sprite* _mainSprite = NULL;
    std::vector< FallingItem* > _fallingItems;
    cocos2d::Size _visibleSize;
    unsigned int _score = 0;
    cocos2d::Label* _scoreLabel;
};

#endif
