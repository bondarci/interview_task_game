#ifndef GameOverScene_h
#define GameOverScene_h

#include "cocos2d.h"

class GameOverScene : public cocos2d::Scene
{
    typedef cocos2d::Scene Super;
    
public:
    virtual bool init();
    
    static cocos2d::Scene *createScene();
    
    void startAgainCallback(cocos2d::Ref *pSender);
    
    CREATE_FUNC(GameOverScene);
};

#endif /* GameOverScene_h */
