#include <stdio.h>
#include "GameOverScene.h"
#include "MainScene.h"

USING_NS_CC;

using namespace std;
using namespace cocos2d::ui;

Scene* GameOverScene::createScene()
{
    
    auto scene = Scene::create();
    GameOverScene* layer = GameOverScene::create();
    
    scene->addChild(layer);
    
    return scene;
}

bool GameOverScene::init()
{
    
    // 1. super init first
    if ( !Super::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto gameOverLabel = Label::createWithTTF("Game over!", "Marker Felt.ttf", 40);
    gameOverLabel->setTextColor(Color4B::RED);
    
    auto startAgainLabel = Label::createWithTTF("Play again", "Marker Felt.ttf", 40);
    startAgainLabel->setTextColor(Color4B::GREEN);
    
    auto playAgainItem = MenuItemLabel::create(startAgainLabel, CC_CALLBACK_1(GameOverScene::startAgainCallback, this));
    
    auto menu = Menu::create(playAgainItem, NULL);
    gameOverLabel->setPosition(visibleSize.width/2, visibleSize.height/2 + gameOverLabel->getContentSize().height * 3);
    menu->setPosition(visibleSize.width/2, gameOverLabel->getPosition().y - gameOverLabel->getContentSize().height);
    
    this->addChild(gameOverLabel);
    this->addChild(menu);
    
    return true;
}

void GameOverScene::startAgainCallback(Ref *pSender)
{
    Director::getInstance()->replaceScene(TransitionFade::create(0.5, MainScene::createScene(), Color3B(255, 255, 255)));
}
