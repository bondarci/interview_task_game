//
//  MainScene.cpp
//  ExpJam
//
//  Created by Two Tails on 09/12/2014.
//
//

#include "MainScene.h"
#include "GameOverScene.h"
#include <algorithm>

USING_NS_CC;

static const int s_bombsCount = 2;
static const int s_flameCount = 3;

static const int s_gemsCount = 4;
static const int s_coinsCount = 4;


//called from another scenes to get back to the game
Scene* MainScene::createScene()
{
    auto scene = Scene::create();
    MainScene* layer = MainScene::create();
    
    scene->addChild(layer);
    
    return scene;
}

// on "init" you need to initialize your instance
bool MainScene::init()
{
    // super init first
    if ( !Super::init() )
    {
        return false;
    }
    
    // get visible size of window
     _visibleSize = Director::getInstance()->getVisibleSize();
    
    // add the main Hero sprite
    float scale = 0.4;
    _mainSprite = Sprite::create( "Leefy-Happy.png" );
    _mainSprite->setPosition( Vec2( _visibleSize.width / 2, _mainSprite->getContentSize().height/2 * scale) );
    _mainSprite->setScale(scale, scale);
    
    // add the sprite as a child to this layer
    this->addChild( _mainSprite );
    
    // add score items and bombs for the main hero - leafy
    
    for (int i = 0; i < s_gemsCount; i++)
    {
        Sprite *sprite = Sprite::create("Gem.png");
        sprite->setScale(scale, scale);
        if (sprite != NULL)
        {
            this->addChild(sprite);
            _fallingItems.emplace_back(new FallingItem(sprite, DangerLevel::save));
        }
    }
    for (int i = 0; i < s_bombsCount; i++)
    {
        Sprite *sprite = Sprite::create("Bomb1.png");
        sprite->setScale(scale, scale);
        if (sprite != NULL)
        {
            this->addChild(sprite);
            _fallingItems.emplace_back(new FallingItem(sprite, DangerLevel::fatal));
            
        }
    }
    for (int i = 0; i < s_flameCount; i++)
    {
        Sprite *sprite = Sprite::create("Flame1.png");
        sprite->setScale(scale, scale);
        if (sprite != NULL)
        {
            this->addChild(sprite);
            _fallingItems.emplace_back(new FallingItem(sprite, DangerLevel::dangerous));
        }
    }
    for (int i = 0; i < s_coinsCount; i++)
    {
        Sprite *sprite = Sprite::create("Coin.png");
        sprite->setScale(scale, scale);
        if (sprite != NULL)
        {
            this->addChild(sprite);
            _fallingItems.emplace_back(new FallingItem(sprite, DangerLevel::save));
        }
    }
    
    // start the items falling
    std::for_each (_fallingItems.begin(), _fallingItems.end(),
                   std::bind1st(std::mem_fun(&MainScene::fallingItemsAction), this));
    
    // put the score on screen
    _scoreLabel = Label::createWithTTF("0","Marker Felt.ttf", 24);
    _scoreLabel->setTextColor(Color4B::WHITE);
    _scoreLabel->setPosition(_visibleSize.width/2, _visibleSize.height/2 + _scoreLabel->getContentSize().height * 3);
    this->addChild(_scoreLabel);
    
    // done
    return true;
}

void MainScene::fallingItemsAction(FallingItem* item)
{
    item->checked = false; // renew the item state to let score again
    
    // generate random position and delay for falling items
    int startX = rand() % static_cast<int>(_visibleSize.width);
    auto delay = DelayTime::create(rand() % 10);
    // todo: rotate action works only first time (shouldn't be difficult to fix)
    auto rotateAction = RotateTo::create(2, 90);
    auto moveAction = MoveTo::create(6, cocos2d::Point(startX,0));
    
    item->sprite->setPosition( Vec2( startX, _visibleSize.height ) );
    
    auto mySpawn = Spawn::createWithTwoActions(rotateAction, moveAction);
    auto actionCallback = CallFunc::create(std::bind(&MainScene::fallingItemsAction, this, item));
    
    auto seq = Sequence::create(delay, mySpawn, actionCallback, delay, nullptr);
    
    item->sprite->runAction(seq);
}

void MainScene::onEnter()
{
    Super::onEnter();
    
    // create a keyboard event listener
    if( _keyEventListener == NULL )
    {
        _keyEventListener = EventListenerKeyboard::create();
        _keyEventListener->onKeyPressed = CC_CALLBACK_2( MainScene::onKeyPressed, this );
        _keyEventListener->onKeyReleased = CC_CALLBACK_2( MainScene::onKeyReleased, this );
    }
    
    // create a mouse event listener
    if( _mouseEventListener == NULL )
    {
        _mouseEventListener = EventListenerMouse::create();
        _mouseEventListener->onMouseMove = CC_CALLBACK_1( MainScene::onMouseMove, this );
        _mouseEventListener->onMouseUp = CC_CALLBACK_1( MainScene::onMouseUp, this );
        _mouseEventListener->onMouseDown = CC_CALLBACK_1( MainScene::onMouseDown, this );
        _mouseEventListener->onMouseScroll = CC_CALLBACK_1( MainScene::onMouseScroll, this );
    }
    
    // register event listeners
    _eventDispatcher->addEventListenerWithSceneGraphPriority( _keyEventListener, this );
    _eventDispatcher->addEventListenerWithSceneGraphPriority( _mouseEventListener, this );
    
    // schedule update calls
    scheduleUpdate();
}

void MainScene::onExit()
{
    Super::onExit();
    
    // de-register event listeners
    _eventDispatcher->removeEventListener( _keyEventListener );
    _eventDispatcher->removeEventListener( _mouseEventListener );
    
    // unschedule update
    unscheduleUpdate();
}

void MainScene::update( float delta )
{
    // called once per frame
    
    // optimization will work here, so there is no much sence calculating these value somewhere outside
    const Vec2 mainSpritePosition = _mainSprite->getPosition();
    float bound = _mainSprite->getBoundingBox().size.width/2 + (*_fallingItems.begin())->sprite->getBoundingBox().size.width/2;
    
    auto checkCollision = [this, bound, mainSpritePosition](FallingItem* item)
    {
        if (item->sprite->getPosition().distance(mainSpritePosition) < bound )
        {
            if (item->level != DangerLevel::save)
            {
                //change scene
                _mainSprite->setTexture("Leefy-Skeleton.png");
                Director::getInstance()->replaceScene(TransitionFade::create(0.5, GameOverScene::createScene(),
                                                                             Color3B(255,0,0)));
            }
            else if (!item->checked)
            {
                ++_score;
                _scoreLabel->setString( std::to_string(_score));
                item->checked = true;
            }
        }
    };
    
    std::for_each (_fallingItems.begin(), _fallingItems.end(), checkCollision);
    
}


#pragma mark - Key Events


void MainScene::onKeyPressed( EventKeyboard::KeyCode keyCode, Event* event )
{
    cocos2d::log( "Key with keycode %d pressed", keyCode );
}

void MainScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event )
{
    cocos2d::log( "Key with keycode %d released", keyCode );
}


#pragma mark - Mouse Events


void MainScene::onMouseDown( Event *event )
{
    // to avoid acceleration we should stop previous movement
    _mainSprite->stopAllActions();
    
    EventMouse* e = (EventMouse*)event;
    float moveDistance = e->getCursorX() -_mainSprite->getPositionX();
    
    auto moveBy = MoveBy::create(2, Vec2(moveDistance, 0));
    
    // would be nice to add check screen bounds, but lets keep it as it is for test version
    _mainSprite->runAction(moveBy);
}

void MainScene::onMouseUp( Event *event )
{
    EventMouse* e = (EventMouse*)event;
    std::string str = "Mouse Up detected, Key: ";
    str += std::to_string(e->getMouseButton());
    cocos2d::log( "%s", str.c_str() );
}

void MainScene::onMouseMove( Event *event )
{
    EventMouse* e = (EventMouse*)event;
    std::string str = "MousePosition X:";
    str += std::to_string(e->getCursorX()) + " Y:" + std::to_string(e->getCursorY());
//    cocos2d::log( "%s", str.c_str() );
}

void MainScene::onMouseScroll( Event *event )
{
    EventMouse* e = (EventMouse*)event;
    std::string str = "Mouse Scroll detected, X: ";
    str += std::to_string(e->getScrollX()) + " Y: " + std::to_string(e->getScrollY());
    cocos2d::log( "%s", str.c_str() );
}
